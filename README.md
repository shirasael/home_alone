# home_alone


**NOTE!!**
**Don't use this repo! use te group found here: https://gitlab.com/home_alone** and open a subgroup for your project :)



Although we are not in our usual work environment, we still are a group of talented, strong, brave coders – with the will and the ability to do good!

This repo is here for you to go wild. Open up a project and share it with your friends in the sector.
Your projects can be about anything you want! UI for an existing system, solutions for the operational shifts, an innovative idea to help people with corona… whatever you believe will bring good to the world :)

So, if you have an idea for a project - go ahead and open a directory in here for it. Don't forget to add a README file so that others will know what it's about.
If you don't have an idea of your own, but like one of the projects in here - feel free to join in and start contributing!

Good luck!
